package cz.vsb.mel0102;

import cz.vsb.mel0102.entities.Paste;
import cz.vsb.mel0102.entities.PasteTag;
import cz.vsb.mel0102.entities.User;
import cz.vsb.mel0102.repositories.PasteRepository;
import cz.vsb.mel0102.repositories.PasteTagRepository;
import cz.vsb.mel0102.repositories.UserRepository;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Singleton
public class Startup {

    @Inject
    UserRepository userRepository;

    @Inject
    PasteTagRepository pasteTagRepository;

    @Inject
    PasteRepository pasteRepository;

    @Transactional
    public void loadUsers(@Observes StartupEvent event) {
        userRepository.deleteAll();

        User user = User.builder()
                .username("MoonSoD")
                .password(BcryptUtil.bcryptHash("12345"))
                .role("admin")
                .build();

        userRepository.persist(user);

        PasteTag pasteTagWeb = PasteTag.builder()
                .label("Web")
                .pastes(Collections.emptyList())
                .build();

        PasteTag pasteTagEmbedded = PasteTag.builder()
                .label("Embedded programming")
                .pastes(Collections.emptyList())
                .build();

        pasteTagRepository.persist(pasteTagWeb);
        pasteTagRepository.persist(pasteTagEmbedded);

        Paste paste = Paste.builder()
                .tags(Arrays.asList(pasteTagWeb))
                .content("class Meta { private static int COUNT = 10; }")
                .user(user)
                .build();

        pasteRepository.persist(paste);
    }

}
