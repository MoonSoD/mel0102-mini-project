package cz.vsb.mel0102.resources;

import cz.vsb.mel0102.dto.PasteTagCreateDto;
import cz.vsb.mel0102.entities.Paste;
import cz.vsb.mel0102.entities.PasteTag;
import cz.vsb.mel0102.repositories.PasteRepository;
import cz.vsb.mel0102.repositories.PasteTagRepository;
import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;

import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.Optional;

@Path("/api/pasteTags")
public class PasteTagResource {

    @Inject
    PasteTagRepository pasteTagRepository;

    @Inject
    PasteRepository pasteRepository;

    @GET
    public Response getAllPasteTags() {
        return Response.ok(pasteTagRepository.listAll()).build();
    }

    @GET
    @Path("/{id}")
    public Response getPasteTagById(@PathParam("id") long id) {
        Optional<PasteTag> pasteTag = pasteTagRepository.findByIdOptional(id);

        if (pasteTag.isEmpty()) {
            return Response.status(404).build();
        }

        return Response.ok(pasteTag.get()).build();
    }

    @POST
    @Authenticated
    @Transactional
    public Response createPasteTag(PasteTagCreateDto pasteTagDto) {
        PasteTag pasteTag = PasteTag.builder()
                .label(pasteTagDto.label)
                .build();

        pasteTagRepository.persist(pasteTag);

        return Response.ok(pasteTagDto).build();
    }

    @DELETE
    @Transactional
    @Path("/{id}")
    public Response deletePasteTag(@PathParam("id") long id) {
        Optional<PasteTag> pasteTag = pasteTagRepository.findByIdOptional(id);

        if (pasteTag.isEmpty()) {
            return Response.status(404).build();
        }

        List<Paste> pastes = pasteRepository.listAll();

        for (Paste paste : pastes) {
            List<PasteTag> pasteTags = paste.getTags();

            if (!pasteTags.isEmpty()) {
                pasteTags.removeIf(tag -> tag.id.equals(id));
                pasteRepository.persist(paste);
            }
        }

        pasteTagRepository.delete(pasteTag.get());

        return Response.ok().build();
    }

}
