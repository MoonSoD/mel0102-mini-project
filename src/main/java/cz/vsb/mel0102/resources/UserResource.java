package cz.vsb.mel0102.resources;

import cz.vsb.mel0102.dto.UserLoginDto;
import cz.vsb.mel0102.dto.UserRegisterDto;
import cz.vsb.mel0102.entities.User;
import cz.vsb.mel0102.repositories.UserRepository;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.security.Authenticated;
import io.smallrye.jwt.build.Jwt;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;

import java.util.Optional;

@Path("/api/users")
public class UserResource {

    @Inject
    UserRepository repository;

    @GET
    @Path("/me")
    public Response me(@Context SecurityContext sc) {
        Optional<User> user = repository.findByUsernameOptional(sc.getUserPrincipal().getName());

        if (user.isEmpty()) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(user.get()).build();
    }

    @POST
    @Transactional
    @Path("/register")
    public Response register(UserRegisterDto userRegisterDto) {
        Optional<User> u = repository.findByUsernameOptional(userRegisterDto.username);

        if (u.isPresent()) {
            return Response.status(Response.Status.CONFLICT).build();
        }

        User user = User.builder()
                .username(userRegisterDto.username)
                .password(BcryptUtil.bcryptHash(userRegisterDto.password))
                .role("user")
                .build();

        repository.persist(user);

        return Response.ok(user).build();
    }

    @GET
    public Response getAllUsers() {
        return Response.ok(repository.listAll()).build();
    }

}
