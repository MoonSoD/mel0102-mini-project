package cz.vsb.mel0102.resources;

import cz.vsb.mel0102.dto.PasteCreateDto;
import cz.vsb.mel0102.dto.PasteUpdateDto;
import cz.vsb.mel0102.entities.Paste;
import cz.vsb.mel0102.entities.PasteTag;
import cz.vsb.mel0102.entities.User;
import cz.vsb.mel0102.repositories.PasteRepository;
import cz.vsb.mel0102.repositories.PasteTagRepository;
import cz.vsb.mel0102.repositories.UserRepository;
import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;

import java.util.*;

@Path("/api/pastes")
public class PasteResource {

    @Inject
    PasteRepository pasteRepository;

    @Inject
    PasteTagRepository pasteTagRepository;

    @Inject
    UserRepository userRepository;

    @GET
    public Response getAllPastes() {
        return Response.ok(pasteRepository.listAll()).build();
    }

    @GET
    @Path("/own")
    public Response getOwnPastes(@Context SecurityContext securityContext) {
        User user = userRepository.findByUsername(securityContext.getUserPrincipal().getName());

        List<Paste> pastes = pasteRepository.list("user", user);

        return Response.ok(pastes).build();
    }

    @GET
    @Path("/{id}")
    public Response getPasteById(@PathParam("id") Long id) {
        Optional<Paste> paste = pasteRepository.findByIdOptional(id);

        if (paste.isEmpty()) {
            return Response.status(404).build();
        }

        return Response.ok(paste.get()).build();
    }

    @POST
    @Authenticated
    @Transactional
    public Response createPaste(PasteCreateDto pasteDto, @Context SecurityContext sc) {
        User user = userRepository.findByUsername(sc.getUserPrincipal().getName());

        Paste paste = Paste.builder()
                .user(user)
                .tags(Collections.emptyList())
                .content(pasteDto.content)
                .build();

        pasteRepository.persist(paste);

        return Response.ok(paste).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Authenticated
    public Response deletePaste(@PathParam("id") Long id, @Context SecurityContext sc) {
        Optional<Paste> paste = pasteRepository.findByIdOptional(id);

        if (paste.isEmpty()) {
            return Response.status(404).build();
        }

        Optional<User> user = userRepository.findByUsernameOptional(sc.getUserPrincipal().getName());

        if (user.isEmpty()) {
            return Response.status(404).build();
        }

        if (!Objects.equals(paste.get().getUser().id, user.get().id)) {
            return Response.status(403).build();
        }

        pasteRepository.delete(paste.get());

        return Response.ok().build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    @Authenticated
    public Response updatePaste(@PathParam("id") Long id, PasteUpdateDto pasteDto, @Context SecurityContext sc) {
        Optional<Paste> paste = pasteRepository.findByIdOptional(id);

        if (paste.isEmpty()) {
            return Response.status(404).build();
        }

        Paste p = paste.get();

        List<String> tags = pasteDto.tags;

        List<PasteTag> newTags = new ArrayList<>();

        for (String tagId : tags) {
            Optional<PasteTag> tag = pasteTagRepository.findByIdOptional(Long.valueOf(tagId));

            if (tag.isEmpty()) {
                return Response.status(404, "Tag " + tagId + " doesnt exist").build();
            }

            newTags.add(tag.get());
        }

        p.setContent(pasteDto.content);
        p.setTags(newTags);

        pasteRepository.persist(p);

        return Response.ok(p).build();
    }

    @PUT
    @Path("/{id}/removeTag/{tagId}")
    @Authenticated
    public Response removeTag(@PathParam("id") Long id, @PathParam("tagId") Long tagId, @Context SecurityContext sc) {
        User user = userRepository.findByUsername(sc.getUserPrincipal().getName());

        Optional<Paste> paste = pasteRepository.findByIdOptional(id);

        if (paste.isEmpty()) {
            return Response.status(404).build();
        }

        if (!Objects.equals(paste.get().getUser().id, user.id)) {
            return Response.status(403).build();
        }

        List<PasteTag> newTags = paste.get()
                .getTags()
                .stream()
                .filter(tag -> !Objects.equals(tag.id, tagId))
                .toList();

        paste.get().setTags(newTags);

        return Response.ok().build();
    }


}
