package cz.vsb.mel0102.repositories;

import cz.vsb.mel0102.entities.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {

    public User findByUsername(String username) {
        return find("username", username).firstResult();
    }

    public Optional<User> findByUsernameOptional(String username) {
        return Optional.ofNullable(findByUsername(username));
    }

}
