package cz.vsb.mel0102.repositories;

import cz.vsb.mel0102.entities.PasteTag;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PasteTagRepository implements PanacheRepository<PasteTag> {}
