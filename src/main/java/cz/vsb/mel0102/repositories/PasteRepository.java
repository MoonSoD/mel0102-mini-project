package cz.vsb.mel0102.repositories;

import cz.vsb.mel0102.entities.Paste;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PasteRepository implements PanacheRepository<Paste> {}
