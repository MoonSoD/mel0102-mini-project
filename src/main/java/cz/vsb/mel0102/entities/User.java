package cz.vsb.mel0102.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.security.jpa.Password;
import io.quarkus.security.jpa.Roles;
import io.quarkus.security.jpa.UserDefinition;
import io.quarkus.security.jpa.Username;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "\"user\"")
@UserDefinition
public class User extends PanacheEntity {

    @Getter
    @Setter
    @Username
    public String username;

    @Getter
    @Setter
    @Password
    @JsonIgnore
    public String password;

    @Getter
    @Setter
    @Roles
    public String role;

    @Getter
    @OneToMany(mappedBy = "user")
    @Fetch(FetchMode.JOIN)
    @JsonIgnore
    public List<Paste> pastes;

    public static void add(String username, String password, String role) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(BcryptUtil.bcryptHash(password));
        user.setRole(role);

        user.persist();
    }
}
