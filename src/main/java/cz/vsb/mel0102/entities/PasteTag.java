package cz.vsb.mel0102.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "paste_tag")
public class PasteTag extends PanacheEntity {

    @Getter
    @Setter
    public String label;

    @ManyToMany(mappedBy = "tags")
    @JsonIgnore
    private List<Paste> pastes;

}
