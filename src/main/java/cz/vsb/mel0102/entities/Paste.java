package cz.vsb.mel0102.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.smallrye.common.constraint.NotNull;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "paste")
public class Paste extends PanacheEntity {

    @Getter
    @ManyToOne
    @Setter
    private User user;

    @Getter
    @Setter
    @Column(nullable = false)
    public String content;

    @Getter
    @Setter
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "paste_tag_paste",
            joinColumns = { @JoinColumn(name = "pastes_id") },
            inverseJoinColumns = { @JoinColumn(name = "tags_id") }
    )
    public List<PasteTag> tags;

    @CreationTimestamp
    public LocalDateTime createdAt;

    @UpdateTimestamp
    public LocalDateTime updatedAt;

}
