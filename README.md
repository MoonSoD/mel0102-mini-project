# pastejar

Rest API for pastebin copy.

## Running the application in dev mode

You can run your application in dev mode that
enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

## Packaging and running the application

To build a fat-jar execute the following command:

```shell script
./mvnw package -Dquarkus.package.jar.type=uber-jar
```

The application, packaged as an fatjar,
is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using:

```shell script
./mvnw package -Dnative
```

Or, if you don't have GraalVM installed,
you can run the native executable build in a container using:

```shell script
./mvnw package -Dnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with:
`./target/pastejar-1.0.0-SNAPSHOT-runner`
